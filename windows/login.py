from PyQt6.QtWidgets import QWidget, QLabel, QVBoxLayout, QCheckBox, QCompleter, QLineEdit, QPushButton, QMessageBox
from PyQt6.QtGui import QFont
from PyQt6.QtCore import Qt, QSize
from modules import account, database
from windows import home

class LoginWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.setFixedSize(QSize(350, 350))
        self.setWindowTitle("Intergalactic")
        vbox = QVBoxLayout()
        vbox.setSpacing(5)
        vbox.setContentsMargins(50, 50, 50, 50)

        heading = QLabel("Intergalactic")
        heading.setFont(QFont("Ariel", 24))
        heading.setAlignment(Qt.AlignmentFlag.AlignCenter)
        vbox.addWidget(heading)

        description = QLabel("Sign up or log in below.")
        description.setAlignment(Qt.AlignmentFlag.AlignCenter)
        description.setContentsMargins(0, 0, 0, 30)
        vbox.addWidget(description)

        self.checkbox = QCheckBox("Create Account")
        self.checkbox.clicked.connect(self.checkbox_clicked)
        vbox.addWidget(self.checkbox, alignment=Qt.AlignmentFlag.AlignCenter)
        vbox.addStretch()

        self.existing_accounts = database.select_accounts("username, token")

        self.username = QLineEdit()
        self.username.setPlaceholderText("Username")
        self.username_completer = QCompleter([i[0] for i in self.existing_accounts])
        self.username_completer.setCaseSensitivity(Qt.CaseSensitivity.CaseInsensitive)
        self.username.setCompleter(self.username_completer)
        self.username.textChanged.connect(self.username_changed)
        vbox.addWidget(self.username)

        self.token = QLineEdit()
        self.token.setPlaceholderText("Token")
        vbox.addWidget(self.token)
        vbox.addStretch()

        self.submit = QPushButton("Continue")
        self.submit.clicked.connect(self.continue_event)
        vbox.addWidget(self.submit)

        self.errors = QLabel()
        self.errors.setStyleSheet("QLabel { color: red; }")
        vbox.addWidget(self.errors)
        self.errors.hide()

        self.setLayout(vbox)
        self.username.setFocus()

    def username_changed(self, event):
        if not self.checkbox.isChecked():
            usernames = [i[0] for i in self.existing_accounts]
            if self.username.text() in usernames:
                self.token.setText(self.existing_accounts[usernames.index(self.username.text())][1])

    def checkbox_clicked(self, event):
        state = self.checkbox.isChecked()
        self.errors.hide()
        if state:
            self.username.setCompleter(None)
            self.token.setDisabled(True)
            self.username.setFocus()
        else:
            self.username.setCompleter(self.username_completer)
            self.token.setDisabled(False)
            self.username.setFocus()

    def refresh_completer(self):
        self.existing_accounts = database.select_accounts("username, token")
        self.username_completer = QCompleter([i[0] for i in self.existing_accounts])
        self.username.setCompleter(self.username_completer)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key.Key_Return:
            self.continue_event()

    def continue_event(self):
        create_account = self.checkbox.isChecked()
        self.errors.hide()
        if create_account:
            if self.username.text() == "":
                self.errors.setText("No username entered.")
                self.errors.show()
                return

            result = account.signup(self.username.text())
            data = result[1]
            if not result[0]:
                dlg = QMessageBox(self)
                dlg.setWindowTitle("Intergalactic")
                dlg.setText("An account has been successfully created for you with the following details. Intergalactic will save these for you, but you should back them up elsewhere too so you don't lose your account.\n\n" + "Username: " + data["username"] + "\nToken: " + data["token"])
                dlg.exec()
                print("Logged in with the following details:", data)

                self.home_window = home.HomeWindow(self)
                self.home_window.show()
                self.close()
            else:
                if data["code"] == 40901:
                    self.errors.setText("User already exists.")
                else:
                    self.errors.setText("Failed to sign up. See logs.")

                self.errors.show()
                print("Failed to log in for the following reason:", result[1])
        else:
            result = account.login(self.token.text())
            data = result[1]
            if not result[0]:
                if data["username"] != self.username.text():
                    dlg = QMessageBox(self)
                    dlg.setWindowTitle("Intergalactic")
                    dlg.setText("The username you entered does not match the username of the token you entered. If it hasn't already been saved, Intergalactic will save this account with the following details.\n\nUsername: " + data["username"] + "\nToken: " + data["token"])
                    dlg.exec()

                print("Logged in with the following details:", data)

                self.home_window = home.HomeWindow(self)
                self.home_window.show()
                self.close()
            else:
                if data["code"] == 40101:
                    self.errors.setText("Invalid token.")
                else:
                    self.errors.setText("Failed to log in. See logs.")

                self.errors.show()
                print("Failed to log in for the following reason:", result[1])
