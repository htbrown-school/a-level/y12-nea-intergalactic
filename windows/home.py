from PyQt6.QtWidgets import QWidget, QLabel, QVBoxLayout, QTabWidget, QGridLayout
from PyQt6.QtCore import Qt
from modules import api, account
from partials import dashboard

class HomeWindow(QWidget):
    def __init__(self, login_window=None):
        super().__init__()
        self.resize(1000, 600)
        self.setWindowTitle("Intergalactic")
        vbox = QVBoxLayout()

        self.session = account.get_session()

        self.tabs = QTabWidget()

        self.dashboard_partial = dashboard.DashboardPartial(self, login_window)
        self.tabs.addTab(self.dashboard_partial, "Dashboard")
        
        self.tabs.addTab(QWidget(), "System")
        self.tabs.addTab(QWidget(), "Travel")

        vbox.addWidget(self.tabs)
        self.setLayout(vbox)

        
        