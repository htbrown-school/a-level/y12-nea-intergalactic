from PyQt6.QtWidgets import QApplication
from PyQt6.QtCore import Qt
from windows import login, home
from modules import account, database
import sys, atexit

app = QApplication(sys.argv)

session = account.get_session()
database.init()

if not session["state"]:
    login_window = login.LoginWindow()
    login_window.show()
else:
    home_window = home.HomeWindow()
    home_window.show()

def on_exit():
    print("Shutting down Intergalactic...")
    database.cleanup()
    print("Goodbye.")

atexit.register(on_exit)
sys.exit(app.exec())
