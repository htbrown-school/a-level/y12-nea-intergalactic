from modules import api, database

session = {
    "state": False, # true = logged in, false = logged out
    "username": None,
    "token": None
}

def get_session():
    global session
    return session

def login(token):
    global session
    result = api.get_account(token)
    data = result[1]

    if not result[0]:
        session = {
            "state": True,
            "username": data["user"]["username"],
            "token": token
        }

        existing_accounts = database.select_accounts("username")
        if (session["username"],) not in existing_accounts:
            database.insert_account(session["username"], session["token"])

        return [False, session]
    else:
        session = {
            "state": False,
            "username": None,
            "token": None
        }
        return [True, data]
    
def signup(username):
    global session
    result = api.create_account(username)
    data = result[1]

    if not result[0]:
        session = {
            "state": True,
            "username": username,
            "token": data["token"]
        }

        existing_accounts = database.select_accounts("username")
        if (session["username"],) not in existing_accounts:
            database.insert_account(session["username"], session["token"])

        return [False, session]
    else:
        session = {
            "state": False,
            "username": None,
            "token": None
        }
        return [True, data]
    
def logout(current_window, login_window):
    global session

    session = {
        "state": False,
        "username": None,
        "token": None
    }

    login_window.show()
    login_window.username.setText("")
    login_window.token.setText("")
    login_window.username.setFocus()
    login_window.refresh_completer()
    login_window.checkbox.setChecked(False)

    current_window.close()
    