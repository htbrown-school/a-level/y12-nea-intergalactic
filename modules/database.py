import sqlite3

conn = sqlite3.connect('./database.db')
cur = conn.cursor()

def init():
    res = cur.execute("SELECT name FROM sqlite_master WHERE name='accounts'")
    if res.fetchone() is None:
        cur.execute("CREATE TABLE accounts(username, token)")

def cleanup():
    print("Closing database connection.")
    conn.close()

def select_accounts(fields):
    res = cur.execute(f"SELECT {fields} FROM accounts")
    data = res.fetchall()
    return data

def insert_account(username, token):
    cur.execute(f"INSERT INTO accounts VALUES ('{username}', '{token}')")
    conn.commit()
    print("Inserted new account into database with details:", str({username, token}))
