import requests
from PyQt6.QtWidgets import QMessageBox

# Standard response from API functions should be in the form below:
# [error: bool, output: any]

API_URL = "https://api.spacetraders.io"

def handle_error(method, uri, err):
    if err["code"] == 42901:
        dlg = QMessageBox()
        dlg.setWindowTitle("Intergalactic")
        dlg.setText("Intergalactic is being rate-limited by SpaceTraders. Please wait a moment before taking anymore actions.")
        dlg.exec()

    print(f"""An error has occurred while performing the following API request:
    {method} {uri}
    Error: {err}""")

def create_account(username):
    uri = f"/users/{username}/claim"
    req = requests.post(API_URL + uri)
    if req.status_code > 400:
        handle_error("POST", uri, req.json()["error"])
        return [True, req.json()["error"]]
    else:
        return [False, req.json()]
    
def get_account(token):
    uri = f"/my/account"
    req = requests.get(API_URL + uri, params={"token": token})
    if req.status_code > 400:
        handle_error("GET", uri, req.json()["error"])
        return [True, req.json()["error"]]
    else:
        return [False, req.json()]
    
def get_ships(token):
    uri = "/my/ships"
    req = requests.get(API_URL + uri, params={"token": token})
    if req.status_code > 400:
        handle_error("GET", uri, req.json()["error"])
        return [True, req.json()["error"]]
    else:
        return [False, req.json()]

def get_ship(token, id):
    uri = f"/my/ships/{id}"
    req = requests.get(API_URL + uri, params={"token": token})
    if req.status_code > 400:
        handle_error("GET", uri, req.json()["error"])
        return [True, req.json()["error"]]
    else:
        return [False, req.json()["ship"]]
    
def get_loans(token):
    uri = "/my/loans"
    req = requests.get(API_URL + uri, params={"token": token})
    if req.status_code > 400:
        handle_error("GET", uri, req.json()["error"])
        return [True, req.json()["error"]]
    else:
        return [False, req.json()]
    
def get_market(token, location):
    uri = f"/locations/{location}/marketplace"
    req = requests.get(API_URL + uri, params={"token": token})
    if req.status_code > 400:
        handle_error("GET", uri, req.json()["error"])
        return [True, req.json()["error"]]
    else:
        return [False, req.json()]