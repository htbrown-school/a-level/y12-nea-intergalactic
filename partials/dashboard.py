from PyQt6.QtWidgets import QWidget, QLabel, QGridLayout, QPushButton, QVBoxLayout, QHBoxLayout, QListWidget, QTableWidget, QTableWidgetItem
from PyQt6.QtGui import QFont
from PyQt6.QtCore import Qt
from modules import api, account

class DashboardPartial(QWidget):
    def __init__(self, parent, login_window):
        super().__init__()
        self.grid = QGridLayout()
        self.session = account.get_session()

        # Account Section
        try:
            account_details = api.get_account(self.session["token"])[1]
        except:
            print("An error occurred while getting account details for the dashboard. Quitting.")
            exit()

        account_vbox = QVBoxLayout()
        account_title_hbox = QHBoxLayout()
        account_title = QLabel(account_details["user"]["username"])
        account_title.setFont(QFont("Ariel", 18))
        account_title_hbox.addWidget(account_title)
        account_refresh = QPushButton("Refresh")
        account_refresh.clicked.connect(self.refresh)
        account_title_hbox.addWidget(account_refresh)
        account_logout = QPushButton("Logout")
        account_logout.clicked.connect(lambda: self.logout(parent, login_window))
        account_title_hbox.addWidget(account_logout)
        account_vbox.addLayout(account_title_hbox)

        self.account_credits = QLabel("Credits: " + str(account_details["user"]["credits"]))
        account_vbox.addWidget(self.account_credits)
        total_loans = sum([i["repaymentAmount"] for i in api.get_loans(self.session["token"])[1]["loans"]])
        self.account_loans = QLabel("Total Loans: " + str(total_loans))
        account_vbox.addWidget(self.account_loans)
        self.account_ships = QLabel("Total Ships: " + str(account_details["user"]["shipCount"]))
        account_vbox.addWidget(self.account_ships)
        self.grid.addLayout(account_vbox, 0, 0, Qt.AlignmentFlag.AlignTop)

        ships_vbox = QVBoxLayout()
        ships_title_hbox = QHBoxLayout()
        ship_count = account_details["user"]["shipCount"]
        self.ships_title = QLabel(f"Ships ({ship_count})")
        self.ships_title.setFont(QFont("Ariel", 18))
        ships_title_hbox.addWidget(self.ships_title)
        ships_title_buy = QPushButton("Buy Ship")
        ships_title_hbox.addWidget(ships_title_buy)

        ships_vbox.addLayout(ships_title_hbox)

        ships_api_data = api.get_ships(self.session["token"])[1]["ships"]
        ships_list_data = [i["manufacturer"] + " " + i["class"] + " in " + i["location"] + " (" + i["id"] + ")" for i in ships_api_data]
        self.ships_list = QListWidget()
        self.ships_list.addItems(ships_list_data)
        self.ships_list.itemPressed.connect(self.refresh)
        if self.ships_list.count() > 0:
            self.ships_list.setCurrentRow(0)
        ships_vbox.addWidget(self.ships_list)

        self.grid.addLayout(ships_vbox, 1, 0, Qt.AlignmentFlag.AlignTop)

        if self.ships_list.count() > 0:
            self.selected_ship = api.get_ship(self.session["token"], ships_api_data[0]["id"])[1]
        
        ship_info_vbox = QVBoxLayout()
        if self.ships_list.count() > 0:
            ship_info_title_hbox = QHBoxLayout()
            self.ship_info_title = QLabel(self.selected_ship["manufacturer"] + " " + self.selected_ship["class"])
            self.ship_info_title.setFont(QFont("Ariel", 18))
            ship_info_title_hbox.addWidget(self.ship_info_title)
            ship_info_title_cargo = QPushButton("View Cargo")
            ship_info_title_hbox.addWidget(ship_info_title_cargo)
            ship_info_title_sell = QPushButton("Sell Ship")
            ship_info_title_hbox.addWidget(ship_info_title_sell)
            ship_info_vbox.addLayout(ship_info_title_hbox)

            ship_info_details_hbox = QHBoxLayout()

            ship_info_details_state_vbox = QVBoxLayout()
            self.ship_info_location = QLabel("Location: " + self.selected_ship["location"])
            ship_info_details_state_vbox.addWidget(self.ship_info_location)
            self.ship_info_cargo = QLabel("Cargo: " + str(self.selected_ship["spaceAvailable"]) + "/" + str(self.selected_ship["maxCargo"]))
            ship_info_details_state_vbox.addWidget(self.ship_info_cargo)
            ship_info_details_hbox.addLayout(ship_info_details_state_vbox)

            ship_info_details_stats_vbox = QVBoxLayout()
            self.ship_info_loading_speed = QLabel("Loading Speed: " + str(self.selected_ship["loadingSpeed"]))
            ship_info_details_stats_vbox.addWidget(self.ship_info_loading_speed)
            self.ship_info_speed = QLabel("Speed: " + str(self.selected_ship["speed"]))
            ship_info_details_stats_vbox.addWidget(self.ship_info_speed)
            self.ship_info_plating = QLabel("Plating: " + str(self.selected_ship["plating"]))
            ship_info_details_stats_vbox.addWidget(self.ship_info_plating)
            self.ship_info_weapons = QLabel("Weapons: " + str(self.selected_ship["weapons"]))
            ship_info_details_stats_vbox.addWidget(self.ship_info_weapons)
            ship_info_details_hbox.addLayout(ship_info_details_stats_vbox)

            ship_info_vbox.addLayout(ship_info_details_hbox)
        else:
            self.ship_info_title = QLabel("No ship selected")
            self.ship_info_title.setFont(QFont("Ariel", 18))
            ship_info_vbox.addWidget(self.ship_info_title)

        self.grid.addLayout(ship_info_vbox, 0, 1, 1, -3, Qt.AlignmentFlag.AlignTop)

        market_vbox = QVBoxLayout()
        market_title_hbox = QHBoxLayout()
        if self.ships_list.count() > 0:
            self.market_title = QLabel("Market (" + self.selected_ship["location"] + ")")
            self.market_title.setFont(QFont("Ariel", 18))
            market_title_hbox.addWidget(self.market_title)
        else:
            self.market_title = QLabel("Market")
            self.market_title.setFont(QFont("Ariel", 18))
            market_title_hbox.addWidget(self.market_title)

        self.market_transaction_button = QPushButton("Make Transaction")
        market_title_hbox.addWidget(self.market_transaction_button)
        market_vbox.addLayout(market_title_hbox)

        self.market_table = QTableWidget()
        self.market_table.setColumnCount(5)
        self.market_table.setHorizontalHeaderLabels(["Name", "Sell Price", "Buy Price", "Amount Available", "Volume"])
        market_vbox.addWidget(self.market_table)

        if self.ships_list.count() > 0:
            self.current_market = api.get_market(self.session["token"], self.selected_ship["location"])[1]["marketplace"]
            for i in self.current_market:
                self.market_table.insertRow(self.market_table.rowCount())
                data = [i["symbol"], i["sellPricePerUnit"], i["purchasePricePerUnit"], i["quantityAvailable"], i["volumePerUnit"]]
                for j in data:
                    item = QTableWidgetItem(str(j))
                    item.setFlags(Qt.ItemFlag.ItemIsEnabled)
                    self.market_table.setItem(self.market_table.rowCount() - 1, data.index(j), item)
        
        self.grid.addLayout(market_vbox, 1, 1, 1, 3, Qt.AlignmentFlag.AlignTop)

        self.setLayout(self.grid)

    def refresh(self):
        if self.ships_list.count() > 0:
            current = self.ships_list.currentItem().text()
            self.refresh_ships()
            self.ships_list.setCurrentItem(self.ships_list.findItems(current, Qt.MatchFlag.MatchExactly)[0])

            self.refresh_ship_details()
            self.refresh_market_details()
        self.refresh_user_details()

    def refresh_ships(self):
        ships_api_data = api.get_ships(self.session["token"])[1]["ships"]
        ships_list_data = [i["manufacturer"] + " " + i["class"] + " in " + i["location"] + " (" + i["id"] + ")" for i in ships_api_data]
        self.ships_list.clear()
        self.ships_list.addItems(ships_list_data)

    def refresh_ship_details(self):
        current = self.ships_list.currentItem().text().strip().split()[4]
        current = current[1:len(current) - 1]
        self.selected_ship = api.get_ship(self.session["token"], current)[1]
        self.ship_info_title.setText(self.selected_ship["manufacturer"] + " " + self.selected_ship["class"])
        self.ship_info_location.setText("Location: " + self.selected_ship["location"])
        self.ship_info_cargo.setText("Cargo: " + str(self.selected_ship["spaceAvailable"]) + "/" + str(self.selected_ship["maxCargo"]))
        self.ship_info_loading_speed.setText("Loading Speed: " + str(self.selected_ship["loadingSpeed"]))
        self.ship_info_speed.setText("Speed: " + str(self.selected_ship["speed"]))
        self.ship_info_plating.setText("Plating: " + str(self.selected_ship["plating"]))
        self.ship_info_weapons.setText("Weapons: " + str(self.selected_ship["weapons"]))

    def refresh_market_details(self):
        self.market_title.setText("Market (" + self.selected_ship["location"] + ")")
        self.current_market = api.get_market(self.session["token"], self.selected_ship["location"])[1]["marketplace"]
        self.market_table.clear()
        self.market_table.setRowCount(0)
        self.market_table.setHorizontalHeaderLabels(["Name", "Sell Price", "Buy Price", "Amount Available", "Volume"])
        for i in self.current_market:
            self.market_table.insertRow(self.market_table.rowCount())
            data = [i["symbol"], i["sellPricePerUnit"], i["purchasePricePerUnit"], i["quantityAvailable"], i["volumePerUnit"]]
            for j in data:
                item = QTableWidgetItem(str(j))
                item.setFlags(Qt.ItemFlag.ItemIsEnabled)
                self.market_table.setItem(self.market_table.rowCount() - 1, data.index(j), item)


    def refresh_user_details(self):
        account_details = api.get_account(self.session["token"])[1]
        self.account_credits.setText("Credits: " + str(account_details["user"]["credits"]))
        self.account_ships.setText("Total Ships: " + str(account_details["user"]["shipCount"]))

        total_loans = sum([i["repaymentAmount"] for i in api.get_loans(self.session["token"])[1]["loans"]])
        self.account_loans.setText("Total Loans: " + str(total_loans))

    def logout(self, parent, login_window):
        account.logout(parent, login_window)